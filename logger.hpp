#pragma once

#include <string>

namespace CompC
{

namespace Logger
{

void log(const std::string &str);

}

}
