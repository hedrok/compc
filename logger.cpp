#include <logger.hpp>

#include <iostream>


namespace CompC
{

namespace Logger
{

void log(const std::string &str)
{
    std::cout << str << std::endl;
}

}

}
